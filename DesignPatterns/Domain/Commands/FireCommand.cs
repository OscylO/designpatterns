﻿using DesignPatterns.Domain.GameObjects;
using DesignPatterns.Domain.GameObjects.Ships;

namespace DesignPatterns.Domain.Commands
{
    public class FireCommand : ICommand
    {
        private readonly Hero _vehicle;

        public FireCommand(Hero vehicle)
        {
            _vehicle = vehicle;
        }

        public void Execute()
        {
            foreach (var projectile in _vehicle.Attack())
            {
                GameWorld.Instance.Initiate(projectile);
            }
        }
    }
}