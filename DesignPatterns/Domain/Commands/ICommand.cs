﻿namespace DesignPatterns.Domain.Commands
{
    public interface ICommand
    {
        void Execute();
    }
}