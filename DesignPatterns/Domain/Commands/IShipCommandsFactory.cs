﻿using System.Collections.Generic;
using DesignPatterns.Domain.GameObjects;
using DesignPatterns.Domain.GameObjects.Ships;
using DesignPatterns.Domain.UserInput;

namespace DesignPatterns.Domain.Commands
{
    public interface IShipCommandsFactory
    {
        IEnumerable<ICommand> Create(Hero ship, Queue<ShipCommandType> commandsSequence);
    }
}