﻿using DesignPatterns.Domain.GameObjects.Ships;

namespace DesignPatterns.Domain.Commands
{
    public class MoveLeftCommand : ICommand
    {
        private readonly IShip _ship;

        public MoveLeftCommand(IShip ship)
        {
            _ship = ship;
        }

        public void Execute() => _ship.Move(MovementDirection.Left);
    }
}