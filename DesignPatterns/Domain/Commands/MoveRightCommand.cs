﻿using DesignPatterns.Domain.GameObjects.Ships;

namespace DesignPatterns.Domain.Commands
{
    public class MoveRightCommand : ICommand
    {
        private readonly IShip _ship;

        public MoveRightCommand(IShip ship)
        {
            _ship = ship;
        }

        public void Execute() => _ship.Move(MovementDirection.Right);
    }
}