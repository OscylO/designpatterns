﻿using System;
using System.Collections.Generic;
using System.Linq;
using DesignPatterns.Domain.GameObjects.Ships;
using DesignPatterns.Domain.UserInput;

namespace DesignPatterns.Domain.Commands
{
    public class ShipCommandsFactory : IShipCommandsFactory
    {
        public IEnumerable<ICommand> Create(Hero ship, Queue<ShipCommandType> commandsSequence)
        {
            return commandsSequence.Select(userCommand => MapUserInputToShipCommand(ship, userCommand));
        }

        private static ICommand MapUserInputToShipCommand(Hero ship, ShipCommandType shipCommandType)
        {
            switch (shipCommandType)
            {
                case ShipCommandType.MoveLeft:
                    return new MoveLeftCommand(ship);
                case ShipCommandType.MoveRight:
                    return new MoveRightCommand(ship);
                case ShipCommandType.Atack:
                    return new FireCommand(ship);
                default:
                    throw new ArgumentOutOfRangeException(nameof(shipCommandType), shipCommandType, null);
            }
        }
    }
}