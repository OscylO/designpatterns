﻿using System;

namespace DesignPatterns.Domain.GameObjects.Components
{
    public class InvaderMovementComponent : IComponent
    {
        public void Add(IComponent component)
        {
            throw new NotSupportedException("This component cannot have children.");
        }

        public void Remove(IComponent component)
        {
            throw new NotSupportedException("This component cannot have children.");
        }

        public void Update(GameObject gameObject, GameWorld gameWorld, Graphics graphics)
        {
            if (!IsChangeOfVelocityNeeded(gameObject, gameWorld))
            {
                return;
            }

            if (IsMovingHorizontaly(gameObject))
            {
                MoveDown(gameObject);
            }
            else
            {
                if (gameWorld.Bounds.IsInBounds(((IVelocity) new Velocity(-1)).ApplyTo(gameObject.Position)))
                {
                    MoveLeft(gameObject);
                }
                else
                {
                    MoveRight(gameObject);
                }
            }
        }

        private static bool IsChangeOfVelocityNeeded(GameObject gameObject, GameWorld gameWorld)
        {
            return !gameWorld.Bounds.IsInBounds(gameObject.Velocity.ApplyTo(gameObject.Position)) ||
                   IsNotMoving(gameObject) ||
                   !IsMovingHorizontaly(gameObject);
        }

        private static void MoveDown(GameObject gameObject) => gameObject.ChangeVelocity(0, 1);

        private static void MoveLeft(GameObject gameObject) => gameObject.ChangeVelocity(-1, 0);

        private static void MoveRight(GameObject gameObject) => gameObject.ChangeVelocity(1, 0);

        private static bool IsMovingHorizontaly(GameObject gameObject) => gameObject.Velocity.Horizontal != 0;

        private static bool IsNotMoving(GameObject gameObject) => gameObject.Velocity.Vertical == 0 && gameObject.Velocity.Horizontal == 0;
    }
}