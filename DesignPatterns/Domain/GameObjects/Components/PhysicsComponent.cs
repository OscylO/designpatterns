using System;
using System.Collections.Generic;

namespace DesignPatterns.Domain.GameObjects.Components
{
    public class PhysicsComponent : IComponent, IObservableSubject<PhysicsComponent>
    {
        private readonly List<ISubjectObserver<PhysicsComponent>> _observers;

        public PhysicsComponent()
        {
            _observers = new List<ISubjectObserver<PhysicsComponent>>();
        }

        public void Add(IComponent component)
        {
            throw new NotSupportedException("This component cannot have children.");
        }

        public void Remove(IComponent component)
        {
            throw new NotSupportedException("This component cannot have children.");
        }

        public void Update(GameObject gameObject, GameWorld gameWorld, Graphics graphics)
        {
            if (gameWorld.ResolveCollisions(gameObject))
            {
                ((IObservableSubject<PhysicsComponent>)this).Notify();
            }

            var newPosition = gameObject.Velocity.ApplyTo(gameObject.Position);

            gameObject.MoveTo(newPosition);
        }

        // IObservableSubject

        void IObservableSubject<PhysicsComponent>.Attach(ISubjectObserver<PhysicsComponent> observer)
        {
            _observers.Add(observer);
        }

        void IObservableSubject<PhysicsComponent>.Detach(ISubjectObserver<PhysicsComponent> observer)
        {
            _observers.Remove(observer);
        }

        void IObservableSubject<PhysicsComponent>.Notify()
        {
            _observers.ForEach(o => o.Update(this));
        }
    }
}