﻿using System;

namespace DesignPatterns.Domain.GameObjects.Components
{
    public class ProjectileMovementComponent : IComponent
    {
        public void Add(IComponent component)
        {
            throw new NotSupportedException("This component cannot have children.");
        }

        public void Remove(IComponent component)
        {
            throw new NotSupportedException("This component cannot have children.");
        }

        public void Update(GameObject gameObject, GameWorld gameWorld, Graphics graphics)
        {
            gameObject.ChangeVelocity(0, -1);
        }
    }
}