﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DesignPatterns.Domain.GameObjects
{
    #region Composit design pattern

    public interface IComponent
    {
        void Update(GameObject gameObject, GameWorld gameWorld, Graphics graphics);
    }

    public class Composite : IComponent
    {
        private readonly List<IComponent> _children = new List<IComponent>();

        public Composite(params Type[] components)
        {
            var notGenericMethods = from componentType in components
                                    let genericMethod = GetType().GetMethod(nameof(AddComponent))
                                    select genericMethod.MakeGenericMethod(componentType);

            foreach (var notGenericMethod in notGenericMethods)
            {
                notGenericMethod.Invoke(this, null);
            }
        }

        public void Add(IComponent component)
        {
            _children.Add(component);
        }

        public T AddComponent<T>() where T : IComponent, new()
        {
            var component = new T();

            Add(component);

            return component;
        }

        public T GetComponent<T>() where T : IComponent, new()
        {
            return _children.OfType<T>().FirstOrDefault();
        }

        public void Remove(IComponent component) => _children.Remove(component);

        public void Update(GameObject gameObject, GameWorld gameWorld, Graphics graphics)
        {
            _children.ForEach(c => c.Update(gameObject, gameWorld, graphics));
        }
    }

    public class MovementComponent : IComponent
    {
        public void Add(IComponent component)
        {
            // Not supported in leaf component.
        }

        public void Remove(IComponent component)
        {
            // Not supported in leaf component.
        }

        public void Update(GameObject gameObject, GameWorld gameWorld, Graphics graphics)
        {
            var newPosition = gameObject.Velocity.ApplyTo(gameObject.Position);

            gameObject.MoveTo(newPosition);
        }
    }

    public class GraphicsComponent : IComponent
    {
        private Sprite _sprite;

        public void ChangeSprite(char symbol, ConsoleColor color)
        {
            _sprite = new Sprite(symbol, color);
        }

        public void Add(IComponent component)
        {
            // Not supported in leaf component.
        }

        public void Remove(IComponent component)
        {
            // Not supported in leaf component.
        }

        public void Update(GameObject gameObject, GameWorld gameWorld, Graphics graphics)
        {
            graphics.ChangeForgroundColor(_sprite.Color);

            if (gameWorld.Bounds.IsInBounds(gameObject.Position))
            {
                _sprite?.Draw(graphics, gameObject.Position);
            }
        }
    };

    #endregion Composit design pattern
}