﻿using System;
using DesignPatterns.Domain.GameObjects.Components;

namespace DesignPatterns.Domain.GameObjects
{
    public class GameObject
    {
        private readonly Composite _compositeComponent;

        private WorldCoordinates _position;
        private Velocity _velocity;

        public IWorldCoordinates Position => _position;

        public IVelocity Velocity => _velocity;

        public bool IsActive { get; private set; }

        public Composite Components => _compositeComponent;

        public GameObject(params Type[] components) 
        {
            _compositeComponent = new Composite(components);

            _position = new WorldCoordinates();
            _velocity = new Velocity();

            IsActive = true;
        }

        public T AddComponent<T>() where T : IComponent, new()
        {
            var component = new T();

            _compositeComponent.Add(component);

            return component;
        }

        public void MoveTo(IWorldCoordinates newPosition)
        {
            _position = _position.Change(newPosition.X, newPosition.Y);
        }

        public void ChangeVelocity(int horizontalVelocity, int verticalVelocity)
        {
            _velocity = _velocity.Change(horizontalVelocity, verticalVelocity);
        }

        public bool Deactivate() => IsActive = false;

        public void Update(GameWorld gameWorld, Graphics graphics)
        {
            if (IsActive)
            {
                _compositeComponent.Update(this, gameWorld, graphics);
            }
        }
    }
}