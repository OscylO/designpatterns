﻿using System;
using DesignPatterns.Domain.GameObjects.Ships;
using DesignPatterns.Domain.Movements;
using DesignPatterns.Domain.Weapons;

namespace DesignPatterns.Domain.GameObjects
{
    public class GameObjectFactory : IGameObjectFactory
    {
        public GameObject Create(GameObjectType newGameObjectType, IWorldCoordinates position)
        {
            GameObject newGameObject;

            switch (newGameObjectType)
            {
                case GameObjectType.Invader:
                    newGameObject = new Invader(new NoFire(), new HorizontalAndVerticalMovement(2));
                    break;
                case GameObjectType.Hero:
                    newGameObject = new Hero(new SemiAutomaticFire());
                    newGameObject.Components.GetComponent<GraphicsComponent>().ChangeSprite('A', ConsoleColor.Magenta);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(newGameObjectType), newGameObjectType, null);
            }

            newGameObject.MoveTo(position);

            return newGameObject;
        }
    }
}