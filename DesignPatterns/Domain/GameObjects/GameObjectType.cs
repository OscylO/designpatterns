﻿namespace DesignPatterns.Domain.GameObjects
{
    public enum GameObjectType
    {
        Invader,
        Hero
    }
}