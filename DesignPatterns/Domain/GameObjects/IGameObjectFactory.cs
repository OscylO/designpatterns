﻿namespace DesignPatterns.Domain.GameObjects
{
    public interface IGameObjectFactory
    {
        GameObject Create(GameObjectType invader, IWorldCoordinates worldCoordinates);
    }
}