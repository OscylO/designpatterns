﻿namespace DesignPatterns.Domain.GameObjects
{
    public interface IObservableSubject<out T>
    {
        void Attach(ISubjectObserver<T> observer);

        void Detach(ISubjectObserver<T> observer);

        void Notify();
    }
}