﻿namespace DesignPatterns.Domain.GameObjects
{
    public interface ISubjectObserver<in T>
    {
        void Update(T value);
    }
}