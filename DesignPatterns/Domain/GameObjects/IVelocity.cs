﻿namespace DesignPatterns.Domain.GameObjects
{
    public interface IVelocity
    {
        int Horizontal { get; }
        int Vertical { get; }
        IWorldCoordinates ApplyTo(IWorldCoordinates position);
    }
}