﻿using System;
using System.Collections.Generic;
using DesignPatterns.Domain.GameObjects.Components;

namespace DesignPatterns.Domain.GameObjects
{
    public class Projectile : GameObject, IObservableSubject<Projectile>, ISubjectObserver<PhysicsComponent>, IProjectile
    {
        private readonly List<ISubjectObserver<Projectile>> _observers;
         
        public Projectile()
        {
            _observers = new List<ISubjectObserver<Projectile>>();

            AddComponent<ProjectileMovementComponent>();

            var collisionComponent = AddComponent<PhysicsComponent>();
            ((IObservableSubject<PhysicsComponent>)collisionComponent).Attach(this);

            AddComponent<GraphicsComponent>().ChangeSprite('|', ConsoleColor.Yellow);
        }

        #region IObservableSubject

        void IObservableSubject<Projectile>.Attach(ISubjectObserver<Projectile> observer)
        {
            _observers.Add(observer);
        }

        void IObservableSubject<Projectile>.Detach(ISubjectObserver<Projectile> observer)
        {
            _observers.Remove(observer);
        }

        void IObservableSubject<Projectile>.Notify()
        {
            _observers.ForEach(o => o.Update(this));
        }

        #endregion


        #region ISubjectObserver

        void ISubjectObserver<PhysicsComponent>.Update(PhysicsComponent value)
        {
            ((IObservableSubject<Projectile>) this).Notify();
        }

        #endregion
    }

    public interface IProjectile
    {
    }
}
