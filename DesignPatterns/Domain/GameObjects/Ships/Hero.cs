﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using DesignPatterns.Domain.GameObjects.Components;
using DesignPatterns.Domain.Movements;
using DesignPatterns.Domain.Weapons;

namespace DesignPatterns.Domain.GameObjects.Ships
{
    public class Hero : GameObject, IShip, ISubjectObserver<Projectile>
    {
        private IFire _weapon;
        private IMove _engine;

        public Hero(IFire weapon)
        {
            AddComponent<MovementComponent>();
            AddComponent<GraphicsComponent>().ChangeSprite('H', ConsoleColor.Cyan);
            AddComponent<ResetUserMovementComponent>();

            _weapon = weapon;
            _engine = new HorizontalMovement(1);
        }

        public void ChangeWeapon(IFire weapon) => _weapon = weapon;

        public void ChangeEngine(IMove engine) => _engine = engine;

        public IEnumerable<Projectile> Attack()
        {
            var projectiles = _weapon.Fire();

            Array.ForEach(projectiles, p =>
            {
                p.MoveTo(new WorldCoordinates(Position.X, Position.Y));

                ((IObservableSubject<Projectile>) p).Attach(this);
            });

            return projectiles;
        }

        public void Move(MovementDirection direction)
        {
            var newVelocity = _engine.Move(direction);

            ChangeVelocity(newVelocity.Horizontal, newVelocity.Vertical);
        }

        void ISubjectObserver<Projectile>.Update(Projectile value)
        {
           Debug.WriteLine("HERO UPDATED BY PROJECTILE");
        }
    }
}
