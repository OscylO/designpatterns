﻿using System.Collections.Generic;
using DesignPatterns.Domain.Weapons;

namespace DesignPatterns.Domain.GameObjects.Ships
{
    public interface IShip
    {
        void Move(MovementDirection direction);

        IEnumerable<Projectile> Attack();

        void ChangeWeapon(IFire weapon);
    }
}