﻿using System;
using System.Collections.Generic;
using DesignPatterns.Domain.GameObjects.Components;
using DesignPatterns.Domain.Movements;
using DesignPatterns.Domain.Weapons;

namespace DesignPatterns.Domain.GameObjects.Ships
{
    public class Invader : GameObject, IShip
    {
        private IFire _weapon;
        private IMove _engine;

        public Invader(IFire weapon, IMove engine) : base(typeof(InvaderMovementComponent), typeof(PhysicsComponent))
        {
            AddComponent<GraphicsComponent>().ChangeSprite('I', ConsoleColor.Red);

            _weapon = weapon;
            _engine = engine;
        }

        public void Move(MovementDirection direction)
        {
            var newVelocity = _engine.Move(direction);

            ChangeVelocity(newVelocity.Horizontal, newVelocity.Vertical);
        }

        public IEnumerable<Projectile> Attack() => _weapon.Fire();

        public void ChangeWeapon(IFire weapon) => _weapon = weapon;

        public void ChangeEngine(IMove engine) => _engine = engine;
    }
}
