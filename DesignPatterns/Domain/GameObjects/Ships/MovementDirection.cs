﻿namespace DesignPatterns.Domain.GameObjects.Ships
{
    public enum MovementDirection
    {
        Up,
        Right,
        Down,
        Left
    }
}