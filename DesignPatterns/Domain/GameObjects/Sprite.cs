﻿using System;

namespace DesignPatterns.Domain.GameObjects
{
    public class Sprite
    {
        public char Symbol { get; }

        public ConsoleColor Color { get; }

        public Sprite(char symbol, ConsoleColor color)
        {
            Symbol = symbol;
            Color = color;
        }

        public void Draw(Graphics graphics, IWorldCoordinates position)
        {
            graphics.Draw(Symbol, position.X, position.Y);
        }
    }
}