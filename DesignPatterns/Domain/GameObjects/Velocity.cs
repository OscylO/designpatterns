﻿namespace DesignPatterns.Domain.GameObjects
{
    public class Velocity : IVelocity
    {
        public int Horizontal { get; }

        public int Vertical { get; }

        public Velocity(int horizontal = 0 , int vertical = 0)
        {
            Horizontal = horizontal;
            Vertical = vertical;
        }

        IWorldCoordinates IVelocity.ApplyTo(IWorldCoordinates position)
        {
            return new WorldCoordinates(position.X + Horizontal, position.Y + Vertical);
        }

        public Velocity Change(int newHorizontal, int newVerical)
        {
            return new Velocity(newHorizontal, newVerical);
        }
    }
}