namespace DesignPatterns.Domain.GameObjects
{
    public class WorldBounds
    {
        public int Top { get; private set; }

        public int Right { get; private set; }

        public int Bottom { get; private set; }

        public int Left { get; private set; }

        public WorldBounds(int top, int right, int bottom, int left)
        {
            Top = top;
            Right = right;
            Bottom = bottom;
            Left = left;    
        }

        public bool IsInBounds(IWorldCoordinates position)
        {
            return position.X >= Left && position.X <= Right && position.Y >= Top && position.Y <= Bottom;
        }
    }
}