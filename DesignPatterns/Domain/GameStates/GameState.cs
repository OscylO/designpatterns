﻿namespace DesignPatterns.Domain.GameStates
{
    public abstract class GameState
    {
        public abstract void Update(IGame game);
    }

    public class GameOverState : GameState
    {
        public override void Update(IGame game)
        {
            throw new System.NotImplementedException();
        }
    }

    internal class GamePausedState : GameState
    {
        public override void Update(IGame game)
        {
            throw new System.NotImplementedException();
        }
    }

    public class GameRunningState : GameState
    {
        public override void Update(IGame game)
        {
            throw new System.NotImplementedException();
        }
    }
}