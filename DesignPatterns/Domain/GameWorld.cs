using System;
using System.Collections.Generic;
using System.Linq;
using DesignPatterns.Domain.GameObjects;
using DesignPatterns.Domain.GameObjects.Ships;

namespace DesignPatterns.Domain
{
    public class GameWorld
    {
        private static volatile GameWorld _instance;
        private static readonly object _syncLock = new object();

        private GameWorld()
        {
            _graphics = new Graphics();
            _gameObjects = new List<GameObject>();
        }

        public static GameWorld Instance
        {
            get
            {
                if (_instance != null)
                {
                    return _instance;
                }

                lock (_syncLock)
                {
                    if (_instance == null)
                    {
                        _instance = new GameWorld();
                    }
                }
                return _instance;
            }
        }

        private readonly Graphics _graphics;
        private readonly List<GameObject> _gameObjects;

        private WorldBounds _bounds;

        public WorldBounds Bounds
        {
            get { return _bounds; }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException(nameof(value));
                }

                if (Bounds != null)
                {
                    throw new InvalidOperationException("Cannot change once defined world's boundaries");
                }

                _bounds = value;
            }
        }

        public void Initiate(GameObject gameObject) => _gameObjects.Add(gameObject);

        public void Destroy(GameObject gameObject) => gameObject.Deactivate();

        public void Update()
        {
            _graphics.Clear();

            _gameObjects.ForEach(go => go.Update(this, _graphics));

            _gameObjects.RemoveAll(go => !go.IsActive);
        }

        public bool ResolveCollisions(GameObject gameObject)
        {
            var collisions = (from otherGameObject in _gameObjects
                              where otherGameObject != gameObject
                              where Intersect(otherGameObject, gameObject)
                              select new Collision(gameObject, otherGameObject))
                             .ToArray();

            foreach (var collision in collisions)
            {
                if (collision.CollidingGameObjects.Any(cgo => cgo is Projectile || cgo is Hero))
                {
                    Array.ForEach(collision.CollidingGameObjects, Destroy);
                }

                if (collision.CollidingGameObjects.Any(cgo => cgo is Hero))
                {
                    //TODO: GAME OVER
                }
            }

            return collisions.Any();
        }

        private static bool Intersect(GameObject gameObject, GameObject otherGameObject)
        {
            var newGameObjectPosition = gameObject.Velocity.ApplyTo(gameObject.Position);
            var newOtherGameObjectPosition = otherGameObject.Velocity.ApplyTo(otherGameObject.Position);

            return newGameObjectPosition.Equals(newOtherGameObjectPosition);
        }

        private class Collision
        {
            public GameObject[] CollidingGameObjects { get; }

            public Collision(GameObject collidingGameObject, GameObject otherCollidingGameObject)
            {
                CollidingGameObjects = new[]{ collidingGameObject, otherCollidingGameObject };
            }
        }
    }
}