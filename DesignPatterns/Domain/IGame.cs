﻿namespace DesignPatterns.Domain
{
    public interface IGame
    {
        void Start();

        void Pause();

        void End();

        void Update();
    }

    public class Game : IGame
    {
        public void Start()
        {
            throw new System.NotImplementedException();
        }

        public void Pause()
        {
            throw new System.NotImplementedException();
        }

        public void End()
        {
            throw new System.NotImplementedException();
        }

        public void Update()
        {
            throw new System.NotImplementedException();
        }
    }
}
