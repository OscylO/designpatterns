﻿using System;

namespace DesignPatterns.Domain
{
    public interface IWorldCoordinates : IEquatable<IWorldCoordinates>
    {
        int X { get; }

        int Y { get; }
    }
}