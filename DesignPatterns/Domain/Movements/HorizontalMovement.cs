using System;
using DesignPatterns.Domain.GameObjects;
using DesignPatterns.Domain.GameObjects.Ships;

namespace DesignPatterns.Domain.Movements
{
    public class HorizontalMovement : IMove
    {
        private readonly int _speed;

        public HorizontalMovement(byte speed)
        {
            _speed = speed;
        }

        public IVelocity Move(MovementDirection direction)
        {
            switch (direction)
            {
                case  MovementDirection.Left:
                    return new Velocity(-1 * _speed);
                case MovementDirection.Right:
                    return new Velocity(_speed);
                case MovementDirection.Up:
                case MovementDirection.Down:
                    return new Velocity();
                default:
                    throw new ArgumentOutOfRangeException(nameof(direction), direction, null);
            }
        }
    }
}