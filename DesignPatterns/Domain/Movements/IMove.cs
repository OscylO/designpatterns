using DesignPatterns.Domain.GameObjects;
using DesignPatterns.Domain.GameObjects.Ships;

namespace DesignPatterns.Domain.Movements
{
    public interface IMove
    {
        IVelocity Move(MovementDirection direction);
    }
}