using System.Collections.Generic;

namespace DesignPatterns.Domain.UserInput
{
    public interface IInput
    {
        Queue<ShipCommandType> ShipCommands { get; }
    }

    public enum ShipCommandType
    {
        MoveLeft,
        MoveRight,
        Atack
    }
}