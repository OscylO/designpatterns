﻿namespace DesignPatterns.Domain.UserInput
{
    public interface IInputDataProvider
    {
        IInput GetInput();
    }
}