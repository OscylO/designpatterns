﻿using System;

namespace DesignPatterns.Domain.UserInput
{
    public class InputDataProvider : IInputDataProvider
    {
        public int MaxInputLength { get; set; } = 10;

        public IInput GetInput()
        {
            var input = new UserInput();
            var keyCount = 0;

            while (Console.KeyAvailable)
            {
                var keyInfo = Console.ReadKey(true);

                if (++keyCount > MaxInputLength)
                {
                    continue;
                }

                switch (keyInfo.Key)
                {
                    case ConsoleKey.LeftArrow:
                        input.StoreInput(ShipCommandType.MoveLeft);
                        break;
                    case ConsoleKey.RightArrow:
                        input.StoreInput(ShipCommandType.MoveRight);
                        break;
                    case ConsoleKey.Spacebar:
                        input.StoreInput(ShipCommandType.Atack);
                        break;
                }
            }

            return input;
        }
    }
}