using System.Collections.Generic;

namespace DesignPatterns.Domain.UserInput
{
    public class UserInput : IInput
    {
        public Queue<ShipCommandType> ShipCommands { get; } = new Queue<ShipCommandType>();

        public void StoreInput(ShipCommandType commandType) => ShipCommands.Enqueue(commandType);
    }
}