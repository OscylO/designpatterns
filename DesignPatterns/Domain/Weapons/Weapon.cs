﻿using System;
using System.Linq;
using DesignPatterns.Domain.GameObjects;

namespace DesignPatterns.Domain.Weapons
{
    #region Strategy Pattern

    public interface IFire
    {
        Projectile[] Fire();
    }

    public class Weapon : IWeapon
    {
        private IFire _fireType;

        public virtual Projectile[] Fire() => _fireType?.Fire();

        public Weapon()
        {
            _fireType = new NoFire();
        }

        public Weapon(IFire fireType)
        {
            _fireType = fireType;
        }

        public void ChangeFireType(IFire fireType)
        {
            _fireType = fireType;
        }
    }

    public class SemiAutomaticFire : IFire
    {
        public Projectile[] Fire() => new[] { new Projectile() };
    }

    public class BurstFire : IFire
    {
        public Projectile[] Fire() => new[] { new Projectile(), new Projectile(), new Projectile() };
    }

    public class NoFire : IFire
    {
        public Projectile[] Fire() => Array.Empty<Projectile>();
    }

    #endregion Strategy Pattern

    #region Decorator Pattern

    public interface IWeapon
    {
        Projectile[] Fire();

        void ChangeFireType(IFire fireType);
    }

    public abstract class WeaponDecorator : IWeapon
    {
        protected readonly IWeapon Decoratee;

        protected WeaponDecorator(IWeapon decoratee)
        {
            if (decoratee == null)
            {
                throw new ArgumentNullException(nameof(decoratee));
            }

            Decoratee = decoratee;
        }

        public virtual Projectile[] Fire()
        {
            return Decoratee.Fire();
        }

        public virtual void ChangeFireType(IFire fireType)
        {
            Decoratee.ChangeFireType(fireType);
        }
    }

    public class TriggerDemagedWeaponDecorator : WeaponDecorator
    {
        private readonly Random _randomNumbers;

        public TriggerDemagedWeaponDecorator(IWeapon decoratee) : base(decoratee)
        {
            _randomNumbers = new Random();
        }

        public override Projectile[] Fire()
        {
            bool failed = _randomNumbers.Next(100) < 20;

            return failed ? Array.Empty<Projectile>() : Decoratee.Fire();
        }
    }

    public class SingleClipWeaponDecorator : WeaponDecorator
    {
        private readonly int _clipCapacity;

        private int _firedProjectileCounter;

        public SingleClipWeaponDecorator(IWeapon decoratee):base(decoratee)
        {
            _clipCapacity = 20;
        }

        public override Projectile[] Fire()
        {
            if (_clipCapacity <= _firedProjectileCounter)
            {
                return Array.Empty<Projectile>();
            }

            int projectilesLeft = _clipCapacity - _firedProjectileCounter;
            var projectilesToFire = Decoratee.Fire().Take(projectilesLeft).ToArray();

            _firedProjectileCounter += projectilesToFire.Length;

            return projectilesToFire;
        }

    }

    #endregion Decorator Pattern
}