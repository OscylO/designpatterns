namespace DesignPatterns.Domain
{
    public class WorldCoordinates : IWorldCoordinates
    {
        public WorldCoordinates(int x = 0, int y = 0)
        {
            X = x;
            Y = y;
        }

        public int X { get; }

        public int Y { get; }

        public WorldCoordinates Change(int x, int y)
        {
            return new WorldCoordinates(x, y);
        }

        public bool Equals(IWorldCoordinates other)
        {
            if (other == null)
            {
                return false;
            }

            return X == other.X && Y == other.Y;
        }

        public override bool Equals(object obj)
        {
            var item = obj as IWorldCoordinates;

            if (item == null)
            {
                return false;
            }

            return X == item.X && Y == item.Y;
        }

        protected bool Equals(WorldCoordinates other)
        {
            return X == other.X && Y == other.Y;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (X*397) ^ Y;
            }
        }
    }
}