﻿using System;

namespace DesignPatterns
{
    public class Graphics
    {
        public Graphics()
        {
            Console.CursorVisible = false;
        }

        public void Clear() => Console.Clear();

        public void Draw(char symbol, int x, int y)
        {
            Console.SetCursorPosition(x, y);
            Console.Write(symbol);
        }

        public void ChangeForgroundColor(ConsoleColor color) => Console.ForegroundColor = color;

        public void ChangeBackgroundColor(ConsoleColor color) => Console.BackgroundColor = color;
    }
}