﻿using System.Linq;
using System.Threading;
using DesignPatterns.Domain;
using DesignPatterns.Domain.Commands;
using DesignPatterns.Domain.GameObjects;
using DesignPatterns.Domain.GameObjects.Ships;
using DesignPatterns.Domain.UserInput;

namespace DesignPatterns
{
    /// <summary>
    /// Project is not even close to being compleated - take it with grain of salt.
    /// </summary>
    class Program
    {
        private static readonly IInputDataProvider _inputDataProvider = new InputDataProvider();
        private static readonly IShipCommandsFactory _commandsFactory = new ShipCommandsFactory();
        private static readonly IGameObjectFactory _gameObjectFactory = new GameObjectFactory();

        private static Hero _hero;
        private static readonly GameWorld GameWorld = GameWorld.Instance;

        static void Main(string[] args)
        {
            Init();

            RunGameLoop();
        }

        private static void Init()
        {
            GameWorld.Bounds = new WorldBounds(0, 50, 20, 0);

            for (int x = 0; x < 20; x++)
            {
                var invader = _gameObjectFactory.Create(GameObjectType.Invader, new WorldCoordinates(x + 5));
                GameWorld.Initiate(invader);
            }

            _hero = (Hero)_gameObjectFactory.Create(GameObjectType.Hero, new WorldCoordinates(20, 20));

            GameWorld.Initiate(_hero);
        }

        private static void RunGameLoop()
        {
            while (true)
            {
                var input = _inputDataProvider.GetInput();
                var commands = _commandsFactory.Create(_hero, input.ShipCommands).ToList();

                commands.ForEach(c => c.Execute());

                GameWorld.Update();

                ControlGameSpeed();
            }
        }

        private static void ControlGameSpeed() => Thread.Sleep(50);
    }
}
